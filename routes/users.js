/**
 * Created by julius on 13/09/14.
 */

var User = require('../models/userModel');
var validate = require('../lib/validate');

var express = require('express');
var router = express.Router();

var config = require('../config/config');

var redis = require('redis');
var redisDb;
if (process.env.NODE_ENV === 'production') {
    var options =  {
        host: config.redisHost,
        port: config.redisPort,
        auth_pass: config.redisPass
    };
    redisDb = redis.createClient(options);
} else {
    redisDb = redis.createClient();
}

var crypto = require('crypto');

var mailgun = require('mailgun').Mailgun;
var mg = new mailgun(config.mgAPIKey);


router.use(validate.credentials);

//TODO: new registration process: 1. request registration with email, 2. register with same email, password and pin from email
router.route('/validate/:pin')
    .post(function (req, res, next) {
        var username = req.body.username, userpass = req.body.password, pin = req.params.pin;
        redisDb.get(username, function (err, result) {
            if (err) return next(err);
            if (!result || result !== pin) {
                return res.status(401).send('PIN wrong.');
            }
            User.findOne({ 'name': username }, 'password', function (err, user) {
                if (err) {
                    return next(err);
                }

                user = new User({
                    name: username,
                    password: userpass
                });

                user.save(function (err) {
                    if (err) {
                        return next(err);
                    }
                    res.status(200).end()
                });
            });
        });
    });

router.route('/register')
    .post(function (req, res, next) {
        var username = req.body.username;
        var pin = randomPin(6);
        redisDb.set(username, pin, function(err, reply) {
            if (err) return next(err);
        });
        redisDb.expire(username, 2 * 60 * 60);   // pin is valid for 2 hours
        if (process.env.NODE_ENV !== 'test'
            || (process.env.NODE_ENV === 'test' && username === 'SOME-USERNAME')) {   // for testing TODO: should not be in here
            var message = 'Hey,\nthanks for using Nononco. Just click the provided link when you ' +
                'on your phone and start no nonsense communication with your friends!\nor enter ' +
                    pin + ' in the verification field.\n\n' +
                'nononco://verify/' + username + '/' + pin;
            mg.sendText('SOME-SENDER-EMAIL-ADDRESS',
                [username],
                'Nononco Registration',
                message,
                config.mgBaseURL,
                {},
                function (err) {
                    if (err) return next(err);
                    return res.status(200).end();
                });
        } else {
            return res.status(200).json({pin: pin}).end();
        }
    });

// TODO: needs to be tested separately
function randomPin (lenght, chars) {
    chars = chars || "0123456789";
    var rnd = crypto.randomBytes(lenght);
    var value = new Array(lenght);
    var len = chars.length;

    for (var i = 0; i < lenght; i++) {
        value[i] = chars[rnd[i] % len];
    }
    return value.join('');
}

//TODO: needs to be tested
router.route('/device')
    .post(function (req, res, next) {
        // TODO: validate. body needs token field.
        var key = req.body.username + ":device";
        redisDb.set(key, req.body.token, function(err, reply) {
            if (err) return next(err);
            return res.status(200).end();
        });
    });


exports = module.exports = router;
