/**
 * Created by julius on 13/09/14.
 */

var Dialog = require('../models/dialogModel');
var User = require('../models/userModel');
var validate = require('../lib/validate');

var config = require('../config/config');
var redis = require('redis');
var redisDb;
if (process.env.NODE_ENV === 'production') {
    var options =  {
        host: config.redisHost,
        port: config.redisPort,
        auth_pass: config.redisPass
    };
    redisDb = redis.createClient(options);
} else {
    redisDb = redis.createClient();
}

var apn = require('apn');

// TODO: add options for development and test environment, env var does not work?
var options = {
    production: false
};
var apnConnection = new apn.Connection(options);

var express = require('express');
var router = express.Router();

router.route('/limit/:limit')
    .get(function (req, res, next) {
        //TODO: what if something goes wrong in this call #1?
        Dialog.getDialogsForUsersWithLimit(req.user.name, req.params.limit, function (err, dialogs) {
            if (err) return next();
            if (!dialogs) return res.status(200).json([]);
            return res.status(200).json(dialogs);
        });
    });

router.route('/pending')
    .get(function (req, res, next) {
        Dialog.getDialogsForUserPending(req.user.name, function (err, dialogs) {
            if (err) return next();
            if (!dialogs) return res.status(200).json([]);
            return res.status(200).json(dialogs);
        });
    });

//TODO route for GET / -> returns all dialogs where user is sender or receiver

router.route('/')
    .post(function (req, res, next) {
        validate.postDialog(req, res, function () {
                //TODO: check if you allowed to send a dialog to the other user
                User.findOne({'name': req.body.receiver}, function (err, receiver) {
                    if (err) return next(err);
                    if (!receiver) return res.status(404).send('User not found!');

                    var dialog = new Dialog({
                        sender: req.user.name,
                        receiver: receiver.name,
                        question: req.body.question
                    });
                    dialog.save(function (err) {
                        if (err) {
                            return next(err);
                        }
                    });

                    // TODO: apn should not be here probably
                    var key = receiver.name + ":device";
                    redisDb.get(key, function (err, result) {
                        if (err) return next(err);
                        if (result != null) {
                            var device = new apn.Device(result);
                            var note = new apn.Notification();
                            note.expiry = Math.floor(Date.now() / 1000) + 3600; // Expires 1 hour from now.
                            note.badge = 1;
                            note.sound = "ping.aiff";
                            note.alert = "\u2049 New Question";
                            note.payload = {'messageFrom': 'Nononco'};
                            apnConnection.pushNotification(note, device);
                        }
                        res.status(200).end(); // TODO: gets send also when dialog.save fails
                    });
                });
            }
        );
    });


router.route('/answer/:dialog_id')
    .put(function (req, res, next) {
        validate.putDialog(req, res, function () {
                //TODO: this should probably happen in DialogModel (as static function)
                Dialog.findOne({'_id': req.params.dialog_id}, function (d_err, dialog) {
                    if (d_err) return next(d_err);
                    if (!dialog) return res.status(404).send('Dialog not found');
                    if (dialog.answer !== null) {
                        return res.status(401).send('Can not alter answer');
                    }
                    if (req.user.name === dialog.receiver) {
                        if (dialog.delivered < 2 && req.body.answer.length !== 0) {
                            dialog.delivered = 2;
                            dialog.answer = req.body.answer;
                            dialog.createdAt = Date();
                            dialog.markModified('answer');
                        }
                    } else {
                        return res.status(401).send('Unauthorized');
                    }
                    var sender = dialog.sender;
                    dialog.save(function (s_err) {
                        if (s_err) return next(s_err);
                        Dialog.findOne({'_id': req.params.dialog_id}, function (r_err, rdialog) {
                            if (r_err) return next(r_err);
                            if (!rdialog) return res.status(500).send('Could not save dialog');

                            // TODO: apn should not be here probably
                            var key = sender + ":device";
                            redisDb.get(key, function (err, result) {
                                if (err) return next(err);
                                if (result != null) {
                                    var device = new apn.Device(result);
                                    var note = new apn.Notification();
                                    note.expiry = Math.floor(Date.now() / 1000) + 3600; // Expires 1 hour from now.
                                    note.badge = 1;
                                    note.sound = "ping.aiff";
                                    note.alert = "\u2049 New Answer";
                                    note.payload = {'messageFrom': 'Nononco'};
                                    apnConnection.pushNotification(note, device);
                                }

                                return res.status(200).json(rdialog);
                            });
                        });
                    });
                });
            }
        );
    });

// TODO: needs to be tested
router.route('/ignore/:dialog_id')
    .post(function (req, res, next) {
        Dialog.findOne({'_id': req.params.dialog_id}, function (d_err, dialog) {
            if (d_err) return next(d_err);
            if (!dialog) return res.status(404).send('Dialog not found');
            if (dialog.receiver === req.user.name) {
                dialog.remove(function (err) {
                    if (err) return next(err);
                    return res.status(200);
                })
            } else {
                return res.status(401).send('Unauthorized!');
            }
        });
    });

//TODO: route PUT for delivery status?

exports = module.exports = router;
