/**
 * Created by julius on 13/09/14.
 */

var AddressBook = require('../models/addressBookModel');
var User = require('../models/userModel');
var validate = require('../lib/validate');
var async = require('async');

var express = require('express');
var router = express.Router();

router.route('/')
    .post(function (req, res, next) {
        validate.postAddressBook(req, res, function () {
            AddressBook.findOne({'user': req.user.id}, function (err, addressBook) {
                if (err) return next(err);

                if (!addressBook) {
                    addressBook = new AddressBook({
                        user: req.user.id,
                        contacts: []
                    });
                } else {
                    addressBook.contacts = []; //TODO: this should not be done, instead return error since an update method should be used!!!!
                }

                for (var i = 0; i < req.body.contacts.length; i++) {
                    addressBook.contacts.push({
                        contactname: req.body.contacts[i],
                        activated: false,
                        blocked: false
                    });
                }
                addressBook.save(function (err) {
                    if (err) {
                        return next(err);
                    }
                    //TODO: User should get flag that he has an AddressBook now!!!
                    res.status(200).end()
                });
            });
        });
    })

    // TODO: what happens on device when user has 2 email adresses??
    // TODO: should be tested!!
    .get(function (req, res, next) {
        AddressBook.findOne({'user': req.user.id}, function (err, addressBook) {
            if (err) return next(err);
            if (!addressBook) {
                addressBook = new AddressBook({
                    user: req.user.id,
                    contacts: []
                });
            }
            var contacts = [];
            async.forEach(addressBook.contacts, function(contact, callback) {
                User.findOne({'name': contact.contactname}, function (u_err, user) {
                    if (user != null) {
                        contacts.push(contact.contactname);
                    }
                    callback();
                });
            }, function (err) {
                if (err) next(err);
                return res.status(200).json(contacts);
            });
        });
    });

//TODO: PUT function

exports = module.exports = router;
