/**
 * Created by julius on 29/07/14.
 */

module.exports = {
    db: process.env["MONGODB_ADDON_URI"],
    dbUser: process.env["MONGODB_ADDON_USER"],
    dbPass: process.env["MONGODB_ADDON_PASSWORD"],

    redisHost: process.env["REDIS_HOST"],
    redisPort: process.env["REDIS_PORT"],
    redisPass: process.env["REDIS_PASSWORD"],

    mgBaseURL: process.env["MG_BASE_URL"],
    mgAPIKey: process.env["MG_API_KEY"],

    apnProduction: process.env["APN_PRODCUTION"]

};
