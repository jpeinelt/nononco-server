/**
 * Created by julius on 29/07/14.
 */

module.exports = {
    db: 'mongodb://URL',
    redisHost: '127.0.0.1',
    redisPort: '6379',
    mgBaseURL: 'mailgunURL',
    mgAPIKey: 'mailgunAPI-Key',

    apnProduction: false
};
