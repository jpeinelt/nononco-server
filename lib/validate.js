/**
 * Created by julius on 29/07/14.
 */


exports.credentials = function (req, res, next) {
    if (!req.body.hasOwnProperty('username') || !req.body.hasOwnProperty('password')) {
        return res.status(400).send('Expected username and password fields.');
    }
    var username = req.body.username, password = req.body.password;
    if (username === '' || password === '') {
        return res.status(400).send('No name or password provided.');
    }
    next();
};

exports.postDialog = function (req, res, next) {
    if (!req.body.hasOwnProperty('receiver') || !req.body.hasOwnProperty('question')) {
        return res.status(400).send('Missing either receiver or question in body.');
    }
    next();
};

exports.putDialog = function (req, res, next) {
    if (!req.body.hasOwnProperty('answer')) {
        return res.status(400).send('Missing answer in body.');
    }
    // TODO: check if answer in body is not too big
    next();
};

exports.postAddressBook = function (req, res, next) {
    if (!req.body.hasOwnProperty('contacts') ) {
        return res.status(400).send('Missing contacts in body.');
    } else if (!Array.isArray(req.body.contacts)) {
        return res.status(400).send('Body malformed.');
    }
    next();
};
