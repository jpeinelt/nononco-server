/**
 * Created by julius on 29/11/14.
 */
require("blanket")({});
process.env.NODE_ENV = 'test';

var supertest = require('supertest');
var app = require('../app');
var request = supertest.agent(app);
var mongoose = require('mongoose');
var async = require('async');
var config = require('../config/config');
var Dialog = require('../models/dialogModel');
var User = require('../models/userModel');
var winston = require('winston');

if (process.env.NODE_ENV !== 'test') {
    winston.log('error', 'Woops, you want NODE_ENV=test before you try this again!');
    process.exit(1);
}

var connect = function () {
    var options = { server: { socketOptions: { keepAlive: 1 } } };
    mongoose.connect(config.db, options);
};

var registerUsers = function (next) {
    var user1 = new User({name: 'user1', password: 'asdfg'});
    var user2 = new User({name: 'user2', password: 'asdfg'});
    var user3 = new User({name: 'user3', password: 'asdfg'});


    async.parallel([
        function (callback) {
            user1.save(function (err, user) {
                callback();
            });
        },
        function (callback) {
            user2.save(function (err, user) {
                callback();
            });
        },
        function (callback) {
            user3.save(function (err, user) {
                callback();
            });
        }
    ], function (err, result) {
        next();
    });
};


suite('AddressBookRestTest', function() {

    setup(function(done) {
        connect();
        for (var i in mongoose.connection.collections) {
            mongoose.connection.collections[i].remove(function() {});
        }

        registerUsers(done);
    });

    suite('#failing', function() {
        test('user not known post', function (done) {
            request
                .post('/addressBooks')
                .auth('tobo', 'learnboost')
                .expect(401)
                .expect('Unauthorized', done);
        });


        test('postMalformedNoContactField', function (done) {
            request
                .post('/addressBooks')
                .auth('user1', 'asdfg')
                .send({stuff: ['user2', 'user3']})
                .expect(400)
                .expect('Missing contacts in body.', done);
        });

        test('postContactFieldMalformed', function (done) {
            request
                .post('/addressBooks')
                .auth('user1', 'asdfg')
                .send({contacts: "blub"})
                .expect(400)
                .expect('Body malformed.', done);
        });
    });

    suite('#working', function () {

        test('postContactField', function (done) {
            request
                .post('/addressBooks')
                .auth('user1', 'asdfg')
                .send({contacts: ['user2', 'user3']})
                .expect(200, function () {
                    request
                        .get('/addressBooks')
                        .auth('user1', 'asdfg')
                        .expect(function (res) {
                            if (res.body.contacts.length !== 2) { // falsy when everything works right
                                return 'res.body.length: ' + res.body.length;
                            }
                        })
                        .expect(200, done);
                });


        })
    });

});


// clean up after tests

teardown(function(done) {
    mongoose.connection.close(function() {
        done();
    });
});


