/**
 * Created by julius on 08/10/14.
 */
require("blanket")({});
process.env.NODE_ENV = 'test';


var supertest = require('supertest');
var app = require('../app');
var request = supertest.agent(app);
var mongoose = require('mongoose');
var async = require('async');
var config = require('../config/config');
var Dialog = require('../models/dialogModel');
var User = require('../models/userModel');

if (process.env.NODE_ENV !== 'test') {
    console.log('Woops, you want NODE_ENV=test before you try this again!');
    process.exit(1);
}

var connect = function () {
    var options = { server: { socketOptions: { keepAlive: 1 } } };
    mongoose.connect(config.db, options);
};



//TODO: clean up setup functions


suite('DialogRest', function () {

    setup(function(done) {
        connect();
        for (var i in mongoose.connection.collections) {
            mongoose.connection.collections[i].remove(function() {});
        }
        done();
    });

    suite('#unauthorized', function() {

        setup(function (done) {
            for (var i in mongoose.connection.collections) {
                mongoose.connection.collections[i].remove(function() {});
            }
            done();
        });

        test('user not known post', function (done) {
            request
                .post('/dialogs')
                .auth('wertzr', 'alfksa')
                .expect(401)
                .expect('Unauthorized', done);
        });
        test('user not known get', function (done) {
            request
                .get('/dialogs')
                .auth('ewrtz', 'asdölfkj')
                .expect(401)
                .expect('Unauthorized', done);
        });
    });

    suite('#postDialog', function () {

        setup(function (done) {

            for (var i in mongoose.connection.collections) {
                mongoose.connection.collections[i].remove(function() {});
            }
            var user1 = new User({name: 'user1', password: 'asdfg'});
            var user2 = new User({name: 'user2', password: 'asdfg'});

            async.series([
                function (callback) {
                    user1.save(callback);
                },
                function (callback) {
                    user2.save(callback);
                }
            ], function (err, result) {
                done();
            });
        });

        test('question has wrong type', function (done) {
            request
                .post('/dialogs/')
                .auth('user1', 'asdfg')
                .send({'receiver': 'user2', 'question': 'blub'})
                .expect(500, done);
        });

        test('receiver unknown', function (done) {
            request
                .post('/dialogs/')
                .auth('user1', 'asdfg')
                .send({'receiver': 'user5', 'question': 'blub'})
                .expect(404, done);
        });

        test('database is empty', function (done) {
            request
                .post('/dialogs/')
                .auth('user1', 'asdfg')
                .send({'receiver': 'user2', 'question': '1'})
                .expect([])
                .expect(200, done);
        });
    });

    suite('#getDialogMethods', function() {
        setup(function (done) {

            for (var i in mongoose.connection.collections) {
                mongoose.connection.collections[i].remove(function() {});
            }

            var dialog1 = {receiver: 'user2', question: '1'};
            var dialog2 = {receiver: 'user2', question: '2'};
            var dialog3 = {receiver: 'user2', question: '3'};

            var user1 = new User({name: 'user1', password: 'asdfg'});
            var user2 = new User({name: 'user2', password: 'asdfg'});

            async.parallel([
                function (callback) {
                    user1.save(callback);
                },
                function (callback) {
                    user2.save(callback);
                }
            ], function (err, result) {
                async.parallel([
                    function (callback) {
                        request
                            .post('/dialogs/')
                            .auth('user1', 'asdfg')
                            .send(dialog1)
                            .expect(200, callback);
                    },
                    function (callback) {
                        request
                            .post('/dialogs/')
                            .auth('user1', 'asdfg')
                            .send(dialog2)
                            .expect(200, callback);
                    },
                    function (callback) {
                        request
                            .post('/dialogs/')
                            .auth('user1', 'asdfg')
                            .send(dialog3)
                            .expect(200, callback);
                    }
                ], function (err, result) {
                    return done();
                });
            });
        });

        test('limit result list with no limit', function (done) {
            request
                .get('/dialogs/limit/')
                .auth('user2', 'asdfg')
                .expect(404, done);
        });

        test('limit result list to 2', function (done) {
            request
                .get('/dialogs/limit/2/')
                .auth('user2', 'asdfg')
                .expect(function (res) {
                    if (res.body.length !== 2) { // falsy when everything works right
                        return 'res.body.length: ' + res.body.length;
                    }
                })
                .expect(200, done);
        });

        test('limit result list not authorized', function (done) {
            request
                .get('/dialogs/limit/user2/20')
                .auth('notRegistered', 'asdfg')
                .expect(401, done);
        });

        //TODO: Test needs improvement: (u1 has 1 pending, get it, u2 sends another, u1 has 1 pending again)
        test('pending for user 1 and 2', function (done) {
            async.series([
                function (callback) {
                    request
                        .get('/dialogs/pending')
                        .auth('user1', 'asdfg')
                        .expect([])
                        .expect(200, callback)
                },
                function (callback) {
                    request
                        .get('/dialogs/pending')
                        .auth('user2', 'asdfg')
                        .expect(function (res) {
                            if (res.body.length !== 3) {
                                return 'res.body.length: ' + res.body.length;
                            }
                        })
                        .expect(200, callback)
                }
            ], function (err, result) {
                done();
            });
        });

    });

    suite('#putDialogMethods', function() {

        var pendingDialogs = {};

        setup(function (done) {

            for (var i in mongoose.connection.collections) {
                mongoose.connection.collections[i].remove(function() {});
            }

            var dialog1 = {receiver: 'user2', question: '1'};
            var dialog2 = {receiver: 'user2', question: '2'};
            var dialog3 = {receiver: 'user2', question: '3'};

            var user1 = new User({name: 'user1', password: 'asdfg'});
            var user2 = new User({name: 'user2', password: 'asdfg'});

            async.parallel([
                function (callback) {
                    user1.save(function (err, user) {
                        callback();
                    });
                },
                function (callback) {
                    user2.save(function (err, user) {
                        callback();
                    });
                }
            ], function (err, result) {
                async.parallel([
                    function (callback) {
                        request
                            .post('/dialogs/')
                            .auth('user1', 'asdfg')
                            .send(dialog1)
                            .expect(200, callback);
                    },
                    function (callback) {
                        request
                            .post('/dialogs/')
                            .auth('user1', 'asdfg')
                            .send(dialog2)
                            .expect(200, callback);
                    },
                    function (callback) {
                        request
                            .post('/dialogs/')
                            .auth('user1', 'asdfg')
                            .send(dialog3)
                            .expect(200, callback);
                    }
                ], function (err, result) {
                    request
                        .get('/dialogs/pending')
                        .auth('user2', 'asdfg')
                        .expect(function (res) {
                            if (res.body.length !== 3) {
                                return 'res.body.length: ' + res.body.length;
                            }
                            pendingDialogs = res.body;
                        })
                        .expect(200, done);
                });
            });
        });

        test('answer a dialog', function (done) {
            request
                .put('/dialogs/answer/' + pendingDialogs[0]._id)
                .auth('user2', 'asdfg')
                .send({ answer: 'some bla bla' })
                .expect(function (res) {
                    if (res.body.delivered !== 2) {
                        return 'dialog delivered status wrong';
                    }
                    if (res.body.answer !== 'some bla bla') {
                        return 'dialog answer wrong';
                    }
                })
                .expect(200, function() {
                    done();
                });

        });
    });

});


// clean up after tests

teardown(function(done) {
    mongoose.connection.close(function() {
        done();
    });
});

