/**
 * Created by julius on 04/09/14.
 */
require("blanket")({});
process.env.NODE_ENV = 'test';

var mongoose = require('mongoose');
var Dialog = require('../models/dialogModel');
var should = require('should');
var config = require('../config/config');


console.log('DialogTest: NODE_ENV: ' + process.env.NODE_ENV + '\n');

if (process.env.NODE_ENV !== 'test') {
    console.log('Woops, you want NODE_ENV=test before you try this again!');
    process.exit(1);
}

var connect = function () {
    var options = { server: { socketOptions: { keepAlive: 1 } } };
    mongoose.connect(config.db, options);
};



suite('Dialog', function() {

    var userName1 = 'some@email.com';
    var userName2 = 'other@email.com';
    var userName3 = 'different@email.com';


    setup(function(done) {
        connect();
        //clear out db
        for (var i in mongoose.connection.collections) {
            mongoose.connection.collections[i].remove(function() {});
        }

        var date1 = new Date();
        date1.setYear(2014);
        date1.setMonth(3);
        date1.setDate(3);

        var date2 = new Date();
        date2.setYear(2014);
        date2.setMonth(6);
        date2.setDate(16);

        var date3 = new Date();
        date3.setYear(2013);
        date3.setMonth(5);
        date3.setDate(5);

        var dialog1 = new Dialog({
            sender: userName1,
            receiver: userName2,
            question: 10,
            createdAt: date1,
            delivered: 1
        });
        var dialog2 = new Dialog({
            sender: userName1,
            receiver: userName3,
            question: 11,
            createdAt: date2,
            delivered: 2
        });
        var dialog3 = new Dialog({
            sender: userName2,
            receiver: userName1,
            question: 12,
            createdAt: date3
        });
        dialog1.save(function (err) {
            should.not.exist(err);
            dialog2.save(function (err) {
                should.not.exist(err);
                dialog3.save(function (err) {
                    should.not.exist(err);
                    done();
                });
            });
        });
    });

    suite('#getDialogsForUsersWithLimit', function() {
        setup(function(done) {
            done();
        });

        // you are testing for errors when checking for properties
        // no need for explicit save test
        test('should return one dialog with sender/reciever 1 and 2', function(done) {
            Dialog.getDialogsForUsersWithLimit(userName2, 1, function (err, dialogs) {
                should.not.exist(err);
                dialogs.should.have.length(1);
                done();
            });
        });
    });


    suite('#getDialogsForUserSinceDate', function() {
        setup(function(done) {
            done();
        });

        // you are testing for errors when checking for properties
        // no need for explicit save test
        test('should return one dialog with createdAt >= 5.5.2014', function(done) {
            var date = new Date();
            date.setYear(2014);
            date.setMonth(5);
            date.setDate(5);
            Dialog.getDialogsForUserSinceDate(userName1, date, function (err, dialogs) {
                should.not.exist(err);
                dialogs.should.have.length(1);
                dialogs[0].createdAt.getFullYear().should.be.above(2013);
                dialogs[0].createdAt.getMonth().should.be.above(4);
                dialogs[0].createdAt.getDate().should.be.above(4);
                done();
            });
        });
    });

    suite('#getDialogsForUserPending', function() {
        setup(function(done) {
            done();
        });

        // you are testing for errors when checking for properties
        // no need for explicit save test
        test('should return two undelivered dialogs', function(done) {
            Dialog.getDialogsForUserPending(userName1, function (err, dialogs) {
                should.not.exist(err);
                dialogs.should.have.length(2);
                for (var i = 0; i < dialogs.length; i++) {
                    if (dialogs[i].delivered === 2) {
                        dialogs[i].should.have.property('sender', userName1);
                    } else if (dialogs[i].delivered === 0) {
                        dialogs[i].should.have.property('receiver', userName1);
                    } else {
                        dialogs[i].should.not.eql(1);
                        dialogs[i].should.not.eql(3);
                    }
                }
                done();
            });
        });
    });

});


// clean up after tests

teardown(function(done) {
    mongoose.connection.close(function() {
        done();
    });
});
