/**
 * Created by julius on 04/09/14.
 */
require("blanket")({});
process.env.NODE_ENV = 'test';

var mongoose = require('mongoose');
var User = require('../models/userModel');
var should = require('should');
var config = require('../config/config');


console.log('UserTest: NODE_ENV: ' + process.env.NODE_ENV + '\n');

if (process.env.NODE_ENV !== 'test') {
    console.log('Woops, you want NODE_ENV=test before you try this again!');
    process.exit(1);
}

var connect = function () {
    var options = { server: { socketOptions: { keepAlive: 1 } } };
    mongoose.connect(config.db, options);
};


suite('User', function() {

    setup(function(done) {
        connect();
        //clear out db
        for (var i in mongoose.connection.collections) {
            mongoose.connection.collections[i].remove(function() {});
        }
        return done();
    });

    suite('#save()', function() {
        var user, emptyPwrdUser;
        var clearPassword = 'password';

        setup(function(done) {
            user = new User({name: 'Test User', password: clearPassword});
            emptyPwrdUser = new User({name: 'Other User', password: ''});
            done();
        });

        test('should have required properties', function(done) {
            user.save(function(err, user) {
                should.not.exist(err);
                user.should.have.property('name', 'Test User');
                user.should.have.property('password');
                user.should.have.property('salt');
                user.password.should.not.eql(clearPassword);
                done();
            });
        });


        test('should not accept empty password', function (done) {
            emptyPwrdUser.save(function(err, user) {
                should.exist(err);
                done();
            });
        });


    });



    suite('#authenticate()', function() {
        var user;
        var clearPassword = 'password';
        // you can use beforeEach in each nested describe
        setup(function(done) {
            user = new User({name: 'Test User', password: clearPassword});
            user.save(function(err, user) {
                should.not.exist(err);
                done();
            });
        });

        test('should authenticate user with correct password', function(done) {
            user.save(function(err, theUser) {
                should.not.exist(err);
                User.getAuthenticated(theUser.name, clearPassword, function(err, authUser) {
                    should.not.exist(err);
                    authUser.should.be.not.false;
                    user.password.should.not.eql(clearPassword);
                    done();
                });
            });
        });
    });

});


// clean up after tests

teardown(function(done) {
    mongoose.connection.close(function() {
        done();
    });
});
