/**
 * Created by julius on 11/09/14.
 */
require("blanket")({});
process.env.NODE_ENV = 'test';

var mongoose = require('mongoose');
var AddressBook = require('../models/addressBookModel');
var User = require('../models/userModel');
var should = require('should');
var config = require('../config/config');


if (process.env.NODE_ENV !== 'test') {
    console.log("Woops, you want NODE_ENV=test before you try this again!");
    process.exit(1);
}


var connect = function () {
    var options = { server: { socketOptions: { keepAlive: 1 } } };
    mongoose.connect(config.db, options);
};


suite('AddressBook', function () {

    setup(function(done) {
        connect();

        var user1 = new User({
            name: 'julius',
            password: 'asdf'
        });

        var user2 = new User({
            name: 'anna',
            password: 'qwer'
        });

        user2.save(function (err) {
            should.not.exist(err);
            user1.save(function (err) {
                should.not.exist(err);
                done();
            });
        });
    });

    suite('#save', function() {
        test('should activate known contacts', function(done) {

            User.findOne({'name': 'julius'}, 'password', function (err, user) {

                should.not.exist(err);
                user.should.be.ok;
                if (user) {
                    var addressBook = new AddressBook({
                        user: user.id,
                        contacts: [
                            {
                                contactname: 'anna',
                                activated: false,
                                blocked: false
                            },
                            {
                                contactname: 'gaby',
                                activated: false,
                                blocked: false
                            }
                        ]
                    });
                    addressBook.save(function (err, savedAddressBook) {
                        should.not.exist(err);
                        if (savedAddressBook) {
                            savedAddressBook.contacts[0].activated.should.be.equal(true);
                            savedAddressBook.contacts[1].activated.should.be.equal(false);
                            done();
                        }
                    });
                }
            });
        });
    });
});

// clean up after tests

teardown(function(done) {
    mongoose.connection.close(function() {
        for (var i in mongoose.connection.collections) {
            mongoose.connection.collections[i].remove(function() {});
        }
        //TODO: dropDatabase instead of remove in setup
        done();
    });
});
