/**
 * Created by julius on 19/09/14.
 */
require("blanket")({});
process.env.NODE_ENV = 'test';

var supertest = require('supertest');
var app = require('../app');
var request = supertest.agent(app);
var mongoose = require('mongoose');
var config = require('../config/config');
var User = require('../models/userModel');
var AddressBook = require('../models/addressBookModel');



console.log('UserRestTest: NODE_ENV: ' + process.env.NODE_ENV + '\n');

if (process.env.NODE_ENV !== 'test') {
    console.log('Woops, you want NODE_ENV=test before you try this again!');
    process.exit(1);
}

var connect = function () {
    var options = { server: { socketOptions: { keepAlive: 1 } } };
    mongoose.connect(config.db, options);
};

suite('UserRest', function () {
    setup(function(done) {
        connect();
        for (var i in mongoose.connection.collections) {
            mongoose.connection.collections[i].remove(function() {});
        }
        done();
    });

    // users

    suite('#testUsersRESTsuccessful', function() {

        var user1 = { username: '0163123456456780', password: 'asdfg'};
        var pin;
        test('register as a new user at /users', function (done) {
            request
                .post('/users/register')
                .send(user1)
                .expect(function (res) {
                    if (!res.body.pin) { // falsy when everything works right
                        return 'should return pin';
                    } else {
                        pin = res.body.pin;
                    }
                })
                .expect(200, function (res) {
                    request
                        .post('/users/validate/' + pin)
                        .send(user1)
                        .expect(200, done);
                });
        });

        var user2 = { username: 'julius@example.de', password: 'asdfg'};
        test('register as new user with email', function (done) {
            request
                .post('/users/register')
                .send(user2)
                .expect(200, done);
        });
    });

    suite('#testUsersRESTerror', function() {

        var user1 = new User({ username : '0164567890', password: 'asdfg'});
        var user2 = { username : '', password: 'asdfg'};
        var user3 = { username : '12345678', password: ''};
        var user4 = { usernomen : '234567', password: 'asdfg'};
        var user5 = { username : '1234567', passworTT: 'asdfg'};
        var user6 = { username : '4354trd', password: 'asdfg'}

        setup(function(done) {
            for (var i in mongoose.connection.collections) {
                mongoose.connection.collections[i].remove(function() {});
            }
            user1.save(function (err, user) {
                done();
            });
        });

        test('register as a new user without username at /users', function(done) {
            request
                .post('/users/register')
                .send(user2)
                .expect(400)
                .expect('No name or password provided.', done);
        });

        test('register as a new user without password at /users', function(done) {
            request
                .post('/users/register')
                .send(user3)
                .expect(400)
                .expect('No name or password provided.', done);

        });

        test('register as a new user with wrong name field at /users', function(done) {
            request
                .post('/users/register')
                .send(user4)
                .expect(400)
                .expect('Expected username and password fields.', done);

        });

        test('register as a new user with wrong pass field at /users', function(done) {
            request
                .post('/users/register')
                .send(user5)
                .expect(400)
                .expect('Expected username and password fields.', done);

        });

        test('validate as a new user with wrong pin', function (done) {
            request
                .post('/users/validate/234')
                .send(user6)
                .expect(401)
                .expect('PIN wrong.', done)
        });
    });
});

// clean up after tests

teardown(function(done) {
    mongoose.connection.close(function() {
        done();
    });
});
