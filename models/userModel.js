/**
 * Created by julius on 29/07/14.
 */

var mongoose = require('mongoose');
var bcrypt = require('bcrypt');


var SALT_WORK_FACTOR = 6;
if (process.env.NODE_ENV === 'test') {
    console.log('ENV: test\nreduced bcrypt factor!!!');
    SALT_WORK_FACTOR = 4;
}


var Schema = mongoose.Schema;

var schema = new Schema({
    name: { type: String, required: true, index: { unique: true } },
    password: { type: String, required: true },
    salt: String,
    hasAddressBook: Boolean
});

schema.pre('save', function(next) {
    var user = this;

    // only hash the password if it has been modified (or is new)
    if (!user.isModified('password')) return next();

    // generate a salt
    bcrypt.genSalt(SALT_WORK_FACTOR, function(err, salt) {
        if (err) return next(err);
        user.salt = salt;
        // hash the password using our new salt
        bcrypt.hash(user.password, salt, function(err, hash) {
            if (err) return next(err);

            // override the cleartext password with the hashed one
            user.password = hash;
            next();
        });
    });
});

// the pre save is not called on findAndUpdate and update (http://mongoosejs.com/docs/middleware.html)
// you might want to have a copy of the pre save function as:
// schema.pre('update', function(next) {


schema.statics.getAuthenticated = function(username, password, next) {
    this.findOne({ name: username }, function(err, user) {
        if (err) return next(err);

        // make sure the user exists
        if (!user) {
            return next(null, false);
        }

        bcrypt.hash(password, user.salt, function(err, hash) {
            // test for a matching password
            if (err) {
                return next(err);
            }
            if (user.password !== hash) {
                return next(null, false);
            }
            return next(null, user);
        });
    });
};

module.exports = mongoose.model('User', schema);
