/**
 * Created by julius on 29/07/14.
 */

var mongoose = require('mongoose');
var UserModel = require('./userModel');
var Schema = mongoose.Schema;

var schema = new Schema({
    user: { type: Schema.ObjectId, ref: 'userModel', required: true },
    contacts: [ {
        contactname: {type: String, required: true},
        activated: {type: Boolean, default: true},
        blocked: {type: Boolean, required: false}
    } ]
});


schema.pre('save', function(next) {
    var addressBook = this;

    var allContactNames = [];
    for (var i = 0; i < addressBook.contacts.length; ++i) {
        allContactNames[i] = addressBook.contacts[i].contactname;
    }

    if (allContactNames.length === 0) {
        return next();
    }
    UserModel.find({'name': {$in: allContactNames} }, function (err, foundContacts) {
        if (err) return next(err);
        if (!foundContacts) {
            return next(null, false);
        }
        for (var i = 0; i < foundContacts.length; i++) {
            var foundContact = foundContacts[i];
            for (var j = 0; j < addressBook.contacts.length; j++) {
                var contact = addressBook.contacts[j];
                if (contact.contactname === foundContact.name) {
                    contact.activated = true;
                    break;
                }
            }
        }
        next();
    });
});



module.exports = mongoose.model('AddressBook', schema);
