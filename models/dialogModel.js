/**
 * Created by julius on 29/07/14.
 */

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var schema = new Schema({
    sender: { type: String, required: true },
    receiver: { type: String, ref: 'userModel', required: true },
    question: { type: Number, required: true },     //TODO: should be String with enum?
    answer: { type: Schema.Types.Mixed, default: null},
    createdAt: { type: Date, default: Date.now },
    delivered: { type: Number, default: 0 },  // 0: only on server, 1: delivered to receiver,
                                              // 2: delivered to receiver and answered to server 3: delivered to sender
    deleted: { type: Number, default: -1},  // -1: not deleted, 0: on server, 1: deleted on sender, 2: deleted on receiver
    expireAfterSeconds: { type: Number, default: 0 }
});

schema.statics.getDialogsForUsersWithLimit = function (userName, limit, next) {
    // both users can be either sender or receiver, limit is to limit the return size
    var query = this.find({ $or:[{sender: userName}, {receiver: userName}] }).limit(limit);
    query.exec(function (err, dialogs) {
        if (err) return next(err);
        dialogs.limit = limit;
        return next(err, dialogs);
    });
};


schema.statics.getDialogsForUserSinceDate = function (userName, date, next) {
    // gets all dialogs that has user as sender or receiver since date
    this.find({ $or:[{sender: userName}, {receiver: userName}], createdAt: {$gte: date} }, function (err, dialogs) {
        if (err) return next(err);
        dialogs.sort({createdAt: -1});
        return next(err, dialogs);
    });
};

//TODO: test has to be improved to check if it always works as it should
schema.statics.getDialogsForUserPending = function (userName, next) {
    // gets all dialogs that are not delivered to user
    this.find({ $or:[{sender: userName, delivered: 2}, {receiver: userName, delivered: 0}, {receiver: userName, delivered: 2}] }, function (err, dialogs) {
        if (err) return next(err);
        dialogs.sort({createdAt: -1});
        return next(err, dialogs);
    });
};


module.exports = mongoose.model('Dialog', schema);
