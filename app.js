/**
 * Created by julius on 29/07/14.
 */


// Module dependencies.

//var https = require('https');
var http = require('http');

var config = require('./config/config');

var express = require('express');
var app = module.exports =  exports.app = express();

var winston = require('winston');

var port = process.env["PORT"] || 8080;

var helmet = require('helmet');
app.use(helmet.noCache());


var mongoose = require('mongoose');
// Connect to mongodb
var connect = function () {
    var mongodb_options;
    if (process.env.NODE_ENV === 'production') {
        mongodb_options = {
            server: {
                socketOptions: { keepAlive: 1 }
            },
            user: config.dbUser,
            pass: config.dbPass
        }
    } else {
        mongodb_options = { server: { socketOptions: { keepAlive: 1 } } };
    }
    mongoose.connect(config.db, mongodb_options);
};
connect();
mongoose.connection.on('error', winston.log);
mongoose.connection.on('disconnected', connect);

var redis = require('redis');
var redisDb;
if (process.env.NODE_ENV === 'production') {
    var options =  {
        host: config.redisHost,
        port: config.redisPort,
        auth_pass: config.redisPass
    };
    redisDb = redis.createClient(options);
} else {
    redisDb = redis.createClient();
}
redisDb.on("error", winston.log);
redisDb.on("connect", winston.log);



var bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());


var passport = require('passport');
var BasicStrategy = require('passport-http').BasicStrategy;
var User = require('./models/userModel');
passport.use(new BasicStrategy(
    function(username, password, done) {
        User.getAuthenticated(username, password, done);
    })
);
app.use(passport.initialize());


var users = require('./routes/users');
app.use('/users', users);
var dialogs = require('./routes/dialogs');
app.use('/dialogs', passport.authenticate('basic', { session: false }), dialogs);
var addressBooks = require('./routes/addressBooks');
app.use('/addressBooks', passport.authenticate('basic', { session: false }), addressBooks);

// create server
//var fs = require('fs');
//var key = fs.readFileSync('./nononco-key.pem');
//var cert = fs.readFileSync('./nononco-cert.pem');
//var https_options = {
//    key: key,
//    cert: cert
//};

//https.createServer(https_options, app).listen(port);


app.listen(port);

console.log("server is running!");
