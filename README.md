# README #

## Overview ##
Nononco is a small chat platform which is inspired by the famous Yo chat app.
While Yo in its orginal form just let users send a short Yo! to each other Nononco goes
a step further. Users can ask others specific questions and the receiver of a question
can answer only exactly this answer. This leads to a NO NONsense COmmunication, hencs the
name Nononco.

## The Problem
While chatting with all those popular platforms is nice and all, sometimes you need
specific information and nothing else. Imagine you want to meet in a park but you
and your friends missed to pick a real meeting point. Now your friends are already in
the park and you arrive. While searching for them you aks for their exact location
and they start to describe it via chat. This gets frustrating easily, but what if
there is a way to force them to give you the exact information you need?

## The Solution
With Nononco you cannot chat with your friends like you do with normal chat platforms.
Instead you can ask specific, predefined questions and you friends only have two 
choices: Ignore your question or answer it in a specific way.

For example you can ask your friend where she is, and if she answers she will 
send you her exact GPS location.
Or if you want to know when to meet your dialoque partner won't be able to start
a discussion but simply to provide a date and time.

## The Platform
Nononco consists of a server and clients. This repository contains the source code
for the server, a simple demo for an iOS client you can find at [Nononco-iOS](https://gitlab.com/jpeinelt/nononco-ios).

The Nononco server is written in Node.js and uses MongoDB for storing data as well
as RedisDB for user-registration purpose. The code has good test coverage, but of course
could be improved, and since it was my first (and last so far) Node.js project, I
probably didn't follow best practices all the time.

### Dependencies ###
This is a more or less complete list of the dependencies, but you can also read the
package.json file...

* Node.js
* MongoDB
* async
* bcryp
* blanket
* body-parser
* express 
* helmet
* mocha
* mongoose
* node-gyp
* passport
* passport-http
* passport-local
* should
* supertest
* winston

### Contribution guidelines ###
Only tested code gets accepted. And of course, the less dependencies, the better.
Otherwise, try to follow best practices and try to improve the code around your
additions with every commit.

### API ###
The API for the server is pretty easy, the only somehow complicated part is the
user registrations since it requires two steps.
TBD.

#### /users ####

POST /

#### /dialogs ####

GET /limit/:receiver/:limit
GET /pending
POST /
PUT /answer/:dialog_id

#### /addressBooks ####

POST /
GET /
